1. Go to the WFGA registration page for the class you wish to import
2. Change the URL so that it ends with ```.api``` instead of ```.html```
2. Open the browser JavaScript console (F12 in Chrome)
3. Copy the following code into the console and press Enter:
    
```
    function stripCData(input) {
    	var length = input.length;
    	return input.substr(9, length - 12);
    }
    function getPlayer(player) {
    	var wfgaId = player.getAttribute("wfga_id");
    	var country = stripCData(player.getElementsByTagName("country_code")[0].innerHTML);
    	var firstName = stripCData(player.getElementsByTagName("first_name")[0].innerHTML);
    	var lastName = stripCData(player.getElementsByTagName("last_name")[0].innerHTML);
    	return {wfgaId, country, firstName, lastName};
    }
    var teams = document.getElementsByTagName("team");
    var output = "";
    for (var i = 0; i < teams.length; i++) {
    	var players = teams[i].getElementsByTagName("player");	
    	if (players.length === 2) {
    		// Doubles
    		var p1 = getPlayer(players[0]);
    		var p2 = getPlayer(players[1]);
    		var country = p1.country === p2.country ? p1.country.toLowerCase() : "*";
    		output += ["", p1.firstName[0] + "." + p1.lastName.toUpperCase() + "/" + p2.firstName[0] + "." + p2.lastName.toUpperCase(), country, p1.wfgaId + ":" + p2.wfgaId].join(";") + "\n";
    	} else if (players.length === 1) {
    		// Singles
    		var player = getPlayer(players[0]);
    		output += [player.firstName, player.lastName.toUpperCase(), player.country.toLowerCase(), player.wfgaId].join(";") + "\n";
    	} else {
    		console.error("Unknown amount of players in team = ", players, teams[i]);
    	}
    }
    console.log(output);
```  

4. Copy the output into Opentour